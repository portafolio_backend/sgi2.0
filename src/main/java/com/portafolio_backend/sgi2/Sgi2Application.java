package com.portafolio_backend.sgi2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sgi2Application {

	public static void main(String[] args) {
		SpringApplication.run(Sgi2Application.class, args);
	}

}
