DB_SGI2

CREATE TABLE estatus(
	id_estatu SERIAL PRIMARY KEY NOT NULL UNIQUE,
	estatu VARCHAR (50) NOT NULL UNIQUE);

INSERT INTO estatus (estatu) VALUES
									('almacen'),
									('asignado'),
									('garantia'),
									('activo'),
									('baja'),
									('vendido');
									
	
CREATE TABLE activos(
	id_activo SERIAL PRIMARY KEY NOT NULL UNIQUE,
	activo VARCHAR (50) NOT NULL UNIQUE);

INSERT INTO activos (activo) VALUES
									('computadora_laptop'),
									('computadora_escritorio'),
									('telefono_fijo'),
									('telefono_celular'),
									('accesorio'),
									('sim');

CREATE TABLE operadores_telefonicos(
	id_operador_telefonico SERIAL PRIMARY KEY NOT NULL UNIQUE,
	operador_telefonico VARCHAR (50) NOT NULL UNIQUE);
						
INSERT INTO operadores_telefonicos (operador_telefonico)  VALUES 
													('Telcel'),
													('Movistar'),
													('Unefon');
									
CREATE TABLE planes_telefonicos (
	id_plan_telefonico SERIAL PRIMARY KEY NOT NULL UNIQUE,
	operador_telefonico_id int NOT NULL,
	plan_telefonico VARCHAR (80) NOT NULL UNIQUE);	

ALTER TABLE planes_telefonicos ADD CONSTRAINT 
	operador_telefonico_id_operadores_telefonicos_id_operador_telefonico FOREIGN KEY (operador_telefonico_id) 
						REFERENCES operadores_telefonicos (id_operador_telefonico) ON DELETE CASCADE ON UPDATE CASCADE;  

INSERT INTO planes_telefonicos (operador_telefonico_id,plan_telefonico)  VALUES 
																	(1,'Telcel Plus'),
																	(1,'Internet Plus'),
																	(1,'Internet en tu Casa Plus'),
																	(2,'Plan ilimiDatos'),
																	(2,'GigaMove 6 GB'),
																	(2,'GigaMove 10 GB');
			
			
CREATE TABLE matrizes (
	id_matriz SERIAL PRIMARY KEY NOT NULL UNIQUE,
	matriz VARCHAR (50) NOT NULL UNIQUE,
	estatu_id int NOT NULL);

ALTER TABLE matrizes ADD CONSTRAINT 
	operador_telefonico_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;     

INSERT INTO matrizes (matriz,estatu_id) VALUES
												('TUTUM',4),
												('PRODEMEX',4),
												('PC&B',4);
			
CREATE TABLE empresas(
	id_empresa SERIAL PRIMARY KEY NOT NULL UNIQUE,
	matriz_id int NOT NULL,
	direccion TEXT NOT NULL,
	estatu_id int NOT NULL);

ALTER TABLE empresas ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;     
			
ALTER TABLE empresas ADD CONSTRAINT 
	matriz_id_matrizes_id_matriz FOREIGN KEY (matriz_id) 
			REFERENCES matrizes (id_matriz) ON DELETE CASCADE ON UPDATE CASCADE;   

ALTER TABLE empresas ADD CONSTRAINT uq_matriz_id_direccion UNIQUE (matriz_id,direccion);

INSERT INTO empresas (matriz_id,direccion,estatu_id) VALUES
												(1,'Av. de los Insurgentes Sur 1811-piso 5, Guadalupe Inn, Álvaro Obregón, 01020 Ciudad de México, CDMX',4),	
												(2,'Av. de los Insurgentes Sur 1811-piso 5, Guadalupe Inn, Álvaro Obregón, 01020 Ciudad de México, CDMX',4),
												(3,'Av. de los Insurgentes Sur 1811-piso 5, Guadalupe Inn, Álvaro Obregón, 01020 Ciudad de México, CDMX',4),
												(2,'Blvd. Solidaridad las Torres S/N, San Jerónimo Chicahualco, Bella Vista, 52170 Toluca de Lerdo, Méx.',4);
					
CREATE TABLE marcas(
	id_marca SERIAL PRIMARY KEY NOT NULL UNIQUE,
	marca VARCHAR (50) NOT NULL,
	activo_id int NOT NULL);                     

ALTER TABLE marcas ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;                            

ALTER TABLE marcas ADD CONSTRAINT uq_marca_activo_id UNIQUE (marca,activo_id);
	
INSERT INTO marcas (marca,activo_id) VALUES
												('DELL',1),
												('DELL',2),
												('HP',1),
												('HP',2),
												('MOTOROLA',4),
												('ASUS',1),
												('AVAYA',3);	
							
CREATE TABLE modelos(
	id_modelo SERIAL PRIMARY KEY NOT NULL UNIQUE,
	modelo varchar (80) NOT NULL,
	marca_id int NOT NULL,
	activo_id int NOT null);
	
ALTER TABLE modelos ADD CONSTRAINT 
	marca_id_marcas_id_marca FOREIGN KEY (marca_id) 
			REFERENCES marcas (id_marca) ON DELETE CASCADE ON UPDATE CASCADE;       

ALTER TABLE modelos ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;       

ALTER TABLE modelos ADD CONSTRAINT uq_modelo_activo_id_marca_id UNIQUE (modelo,activo_id,marca_id);

INSERT INTO modelos (modelo,marca_id,activo_id) VALUES
												('LATITUDE 15 3520 4G6X1',1,1),
												('LATITUDE 14 3420',1,1),
												('LATITUDE 14 7420 R5CTN',1,1),
												('VOSTRO 14 3400 66RJ1',1,1),
												('PC ARMADA',2,2),
												('PC ARMADA',2,4),
												('G8 POWER',5,4),
												('G65',5,4),
												('EDGE 100 POWER',5,4),
												('G82',5,4),
												('K175',7,3);	


CREATE TABLE computadoras(
	id_computadora SERIAL PRIMARY KEY NOT NULL UNIQUE,
	empresa_id int NOT NULL,
	id_equipo VARCHAR (50) NOT NULL UNIQUE,	
	activo_id int NOT NULL,
	num_serie bigint NOT NULL,
	nombre_equipo VARCHAR(50) NOT NULL UNIQUE,
	marca_id int NOT NULL,
	modelo_id int NOT null,
	caracteristicas TEXT NOT NULL,
	costo FLOAT NOT NULL,
	fecha_alta DATE NOT NULL,
	fecha_garantia DATE NOT NULL,
	fecha_vida DATE NOT NULL,
	fecha_baja DATE,
	observaciones TEXT NOT NULL,
	estatu_id int NOT NULL);
	
ALTER TABLE computadoras ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE computadoras ADD CONSTRAINT 
	marca_id_marcas_id_marca FOREIGN KEY (marca_id) 
			REFERENCES marcas (id_marca) ON DELETE CASCADE ON UPDATE CASCADE;     

ALTER TABLE computadoras ADD CONSTRAINT 
	modelo_id_modelos_id_modelo FOREIGN KEY (modelo_id) 
			REFERENCES modelos (id_modelo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE computadoras ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;     

ALTER TABLE computadoras ADD CONSTRAINT 
	empresa_id_empresas_id_empresa FOREIGN KEY (empresa_id) 
			REFERENCES empresas (id_empresa) ON DELETE CASCADE ON UPDATE CASCADE;    
		
CREATE TABLE sims (
	id_sim SERIAL PRIMARY KEY NOT NULL UNIQUE,
	empresa_id int NOT NULL,
	activo_id int NOT NULL,
	num_serie bigint NOT NULL,
	operador_telefonico_id int NOT NULL,
	plan_telefonico_id int NOT null,
	numero_telefonico varchar(10) NOT NULL UNIQUE,
	costo FLOAT NOT NULL,
	fecha_alta DATE NOT NULL,
	fecha_baja DATE,
	estatu_id int NOT NULL);
		
ALTER TABLE sims ADD CONSTRAINT 
	empresa_id_empresas_id_empresa FOREIGN KEY (empresa_id) 
			REFERENCES empresas (id_empresa) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE sims ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE sims ADD CONSTRAINT 
	operador_telefonico_id_operadores_telefonicos_id_operador_telefonico FOREIGN KEY (operador_telefonico_id) 
						REFERENCES operadores_telefonicos (id_operador_telefonico) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE sims ADD CONSTRAINT 
	plan_telefonico_id_planes_telefonicos_id_plan_telefonico FOREIGN KEY (plan_telefonico_id) 
						REFERENCES planes_telefonicos (id_plan_telefonico) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE sims ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;   

CREATE TABLE telefonos (
	id_telefono SERIAL PRIMARY KEY NOT NULL UNIQUE,
	empresa_id int NOT NULL,
	activo_id int NOT NULL,
	num_serie bigint NOT NULL,
	marca_id int NOT NULL,
	modelo_id int NOT null,
	sim_id int NOT NULL,
	caracteristicas TEXT NOT NULL,
	costo FLOAT NOT NULL,
	fecha_alta DATE NOT NULL,
	fecha_garantia DATE NOT NULL,
	fecha_vida DATE NOT NULL,
	fecha_baja DATE,
	observaciones TEXT NOT NULL,
	estatu_id int NOT NULL);

ALTER TABLE telefonos ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE telefonos ADD CONSTRAINT 
	marca_id_marcas_id_marca FOREIGN KEY (marca_id) 
			REFERENCES marcas (id_marca) ON DELETE CASCADE ON UPDATE CASCADE;     

ALTER TABLE telefonos ADD CONSTRAINT 
	modelo_id_modelos_id_modelo FOREIGN KEY (modelo_id) 
			REFERENCES modelos (id_modelo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE telefonos ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE telefonos ADD CONSTRAINT 
	empresa_id_empresas_id_empresa FOREIGN KEY (empresa_id) 
			REFERENCES empresas (id_empresa) ON DELETE CASCADE ON UPDATE CASCADE;    
			
ALTER TABLE telefonos ADD CONSTRAINT 
	sim_id_sims_id_sim FOREIGN KEY (sim_id) 
			REFERENCES sims (id_sim) ON DELETE CASCADE ON UPDATE CASCADE;    

CREATE TABLE accesorios (
	id_accesorio SERIAL PRIMARY KEY NOT NULL UNIQUE,
	empresa_id int NOT NULL,
	activo_id int NOT NULL,
	num_serie bigint NOT NULL,
	marca_id int NOT NULL,
	modelo_id int NOT null,
	caracteristicas TEXT NOT NULL,
	costo FLOAT NOT NULL,
	fecha_alta DATE NOT NULL,
	fecha_garantia DATE NOT NULL,
	fecha_vida DATE NOT NULL,
	fecha_baja DATE,
	observaciones TEXT NOT NULL,
	estatu_id int NOT NULL);

ALTER TABLE accesorios ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE accesorios ADD CONSTRAINT 
	marca_id_marcas_id_marca FOREIGN KEY (marca_id) 
			REFERENCES marcas (id_marca) ON DELETE CASCADE ON UPDATE CASCADE;     

ALTER TABLE accesorios ADD CONSTRAINT 
	modelo_id_modelos_id_modelo FOREIGN KEY (modelo_id) 
			REFERENCES modelos (id_modelo) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE accesorios ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE accesorios ADD CONSTRAINT 
	empresa_id_empresas_id_empresa FOREIGN KEY (empresa_id) 
			REFERENCES empresas (id_empresa) ON DELETE CASCADE ON UPDATE CASCADE;    

//

CREATE TABLE departamentos(
	id_departamento SERIAL PRIMARY KEY NOT NULL UNIQUE,
	departamento VARCHAR(50) NOT NULL UNIQUE,
	empresa_id int NOT NULL);

ALTER TABLE departamentos ADD CONSTRAINT 
	empresa_id_empresas_id_empresa FOREIGN KEY (empresa_id) 
			REFERENCES empresas (id_empresa) ON DELETE CASCADE ON UPDATE CASCADE;    
	
ALTER TABLE departamentos ADD CONSTRAINT uq_departamento_empresa_id UNIQUE (departamento,empresa_id);

CREATE TABLE perfiles(
	id_perfil SERIAL PRIMARY KEY NOT NULL UNIQUE,
	perfil VARCHAR(50) NOT NULL UNIQUE);

CREATE TABLE empleados(
	id_empleado SERIAL PRIMARY KEY NOT NULL UNIQUE,
	primer_nombre varchar (60) NOT NULL,
	segundo_nombre varchar(50) NOT NULL,
	primer_apellido varchar (60) NOT NULL,
	segundo_apellido varchar(50) NOT NULL,
	num_seg_social bigint NOT NULL UNIQUE,
	perfil_id int NOT NULL,
	departamento_id int NOT NULL,
	fecha_ingreso DATE NOT NULL,
	fecha_baja DATE NOT NULL,
	estatu_id int NOT NULL);


ALTER TABLE empleados ADD CONSTRAINT 
	departamento_id_departamentos_id_departamento FOREIGN KEY (departamento_id) 
			REFERENCES departamentos (id_departamento) ON DELETE CASCADE ON UPDATE CASCADE;       
	
ALTER TABLE empleados ADD CONSTRAINT 
	perfil_id_perfiless_id_perfil FOREIGN KEY (perfil_id) 
			REFERENCES perfiles (id_perfil) ON DELETE CASCADE ON UPDATE CASCADE;     
	
ALTER TABLE empleados ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;    

ALTER TABLE empleados ADD CONSTRAINT uq_primer_nombre_segundo_nombre_primer_apellido_segundo_apellido UNIQUE 
(primer_nombre,segundo_nombre,primer_apellido,segundo_apellido);


CREATE TABLE contrasenas(
	id_contraseña SERIAL PRIMARY KEY NOT NULL UNIQUE,
	empleado_id int NOT null UNIQUE,
	password varchar(200) NOT NULL);
	
ALTER TABLE contrasenas ADD CONSTRAINT 
	empleado_id_empleados_id_empleado FOREIGN KEY (empleado_id) 
			REFERENCES empleados (id_empleado) ON DELETE CASCADE ON UPDATE CASCADE;  



CREATE TABLE asignaciones(
	id_asignacion SERIAL PRIMARY KEY NOT NULL UNIQUE,
	activo_id int NOT NULL,
	serie bigint NOT NULL,
	estatu_id int NOT NULL,
	obseravaciones TEXT NOT NULL,
	fecha_asignacion DATE NOT NULL,
	fecha_desasignacion DATE,
	tecnico_id int NOT NULL,
	empleado_id int NOT NULL,
	hoja_resguardo TEXT);
	
ALTER TABLE asignaciones ADD CONSTRAINT 
	activo_id_activos_id_activo FOREIGN KEY (activo_id) 
			REFERENCES activos (id_activo) ON DELETE CASCADE ON UPDATE CASCADE;        
		
ALTER TABLE asignaciones ADD CONSTRAINT 
	estatu_id_estatus_id_estatu FOREIGN KEY (estatu_id) 
			REFERENCES estatus (id_estatu) ON DELETE CASCADE ON UPDATE CASCADE;   
			
ALTER TABLE asignaciones ADD CONSTRAINT 
	empleado_id_empleados_id_empleado FOREIGN KEY (empleado_id) 
			REFERENCES empleados (id_empleado) ON DELETE CASCADE ON UPDATE CASCADE;    
 
ALTER TABLE asignaciones ADD CONSTRAINT 
	tecnico_id_empleados_id_empleado FOREIGN KEY (tecnico_id) 
			REFERENCES empleados (id_empleado) ON DELETE CASCADE ON UPDATE CASCADE;    
			
			


